from django import forms
from .models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description"]
        labels = {"name": "Project Name", "description": "Project Description"}


class ProjectForm2(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
